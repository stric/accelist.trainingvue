﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TrainingVue.Models
{
    public class BookForm
    {
        public string Author { get; set; }

        public string Title { get; set; }
    }
}
