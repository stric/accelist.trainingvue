﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TrainingVue.Models
{
    public class DataGridModel<T>
    {
        public List<T> Data { get; set; }
    }
}
