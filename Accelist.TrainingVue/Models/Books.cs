﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TrainingVue.Models
{
    public class Books
    {
        public Books(int id, string author, string title)
        {
            this.Author = author;
            this.Id = id;
            this.Title = title;
        }

        public int Id { get; set; }

        public string Author { get; set; }

        public string Title { get; set; }


    }
}
