﻿export interface DataGridModel {
    headers: DataGridHeader[];
    data: object[];
}

export interface DataGridHeader {
    columnName: string;
}


export interface DataGridBooks {
    title: string;
    author: string;
    id: number;
}

export interface BooksForm {
    title: string;
    author: string;
}

export class DataGridBooksClass {
    title2!: string;
}
