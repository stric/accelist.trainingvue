﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accelist.TrainingVue.Models;
using Accelist.TrainingVue.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Accelist.TrainingVue.API
{
    [Route("api/books")]
    [ApiController]
    public class BookApiController : ControllerBase
    {
        private readonly BooksService BookService;

        public BookApiController(BooksService bookService)
        {
            this.BookService = bookService;
        }

        // GET: api/BookApi
        [HttpGet("getbooks")]
        public ActionResult<List<Books>> Get()
        {
            var result = this.BookService.GetDataProducts();
            return result;
        }

        [HttpGet("getproducts")]
        public ActionResult GetProducts()
        {
            var result = this.BookService.GetProducts();
            return Ok(result);
        }

        // GET: api/BookApi/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/BookApi
        [HttpPost]
        public ActionResult Post([FromBody]  BookForm param)
        {
            var response = this.BookService.AddProduct(param);
            if (string.IsNullOrEmpty(response))
            {
                return BadRequest(response);
            }

            return Ok();

        }

        // PUT: api/BookApi/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
