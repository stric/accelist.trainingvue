﻿using Accelist.TrainingVue.Data;
using Accelist.TrainingVue.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TrainingVue.Services
{

    public class BooksService
    {
        public BooksService(DataProduct dataProduct)
        {
            this.DataProduct = dataProduct;
        }
        private readonly DataProduct DataProduct;

        public List<Books> GetDataProducts()
        {
            return DataProduct.Books;
        }

        public DataGridModel<Books> GetProducts()
        {
            var result = new DataGridModel<Books>
            {
                Data = DataProduct.Books
            };
            return result;
        }


        public string AddProduct(BookForm param)
        {
            var isAny = DataProduct.Books.Where(q => q.Title == param.Title).Any();
            if (isAny)
            {
                return "Data sudah di pakai";
            }

            var currentId = DataProduct.Books.OrderByDescending(q => q.Id).FirstOrDefault().Id;
            DataProduct.Books.Add(new Books(currentId + 1, param.Author, param.Title));
            return null;

        }
    }
}
