﻿using Accelist.TrainingVue.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.TrainingVue.Data
{
    public class DataProduct
    {
        public List<Books> Books { get; set; } = new List<Books>
        {
            new Books(1,"Jack","Titanic"),
            new Books(2,"Echiro Oda", "One Piece"),
            new Books(3,"Mashashi Kishimoto", "Naruto"),
            new Books(4,"Shincan", "No name")
        };
    }
}
